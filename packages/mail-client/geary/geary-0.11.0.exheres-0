# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ vala_dep=true ]
require gsettings gtk-icon-cache freedesktop-desktop
require cmake [ api=2 ]

SUMMARY="A lightweight email program designed around conversations"
DESCRIPTION="
Geary is a new email reader for GNOME designed to let you read your email quickly and effortlessly.
Its interface is based on conversations, so you can easily read an entire discussion without having
to click from message to message.
"
HOMEPAGE="http://yorba.org/geary/"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-util/desktop-file-utils
        dev-util/intltool[>=0.35.0]
        gnome-desktop/gnome-doc-utils
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
    build+run:
        base/libgee:0.8[>=0.8.5]
        dev-db/sqlite:3[>=3.7.4]
        dev-libs/glib:2[>=2.38]
        dev-libs/libsecret:1[>=0.11]
        dev-libs/libxml2:2.0[>=2.7.8]
        gnome-desktop/gcr[>=3.10.1][vapi]
        media-libs/libcanberra[>=0.28]
        net-libs/webkit:3.0[>=2.3.0][gobject-introspection]
        net-utils/gmime:2.6[>=2.6.14]
        x11-libs/cairo[X]
        x11-libs/gdk-pixbuf:2.0[X]
        x11-libs/gtk+:3[>=3.10.0]
        x11-libs/libnotify[>=0.7.5]
        x11-libs/pango[X]
"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/${PN}-Use-PKG_CONFIG_EXECUTABLE-provided-by-find_package-P.patch
    "${FILES}"/0001-Use-GNUInstallDirs-cmake-module-for-controlling-inst.patch
)

src_prepare() {
    cmake_src_prepare

    # Don't install to $(pkg-config glib-2.0 --variable prefix)/share/...
    edo sed -e "/set(GSETTINGS_DIR/s:\${_glib_prefix}:/usr:" \
            -i cmake/GSettings.cmake
}

src_configure() {
    # can't do this in global scope because of VALAC
    local myconf=(
        -DCMAKE_INSTALL_BINDIR="/usr/$(exhost --target)/bin"
        -DCMAKE_INSTALL_PREFIX=/usr
        -DDESKTOP_UPDATE:BOOL=OFF
        -DDESKTOP_VALIDATE:BOOL=OFF
        -DICON_UPDATE:BOOL=OFF
        -DVALA_EXECUTABLE:FILEPATH=${VALAC}
    )

    ecmake "${myconf[@]}"
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
}

